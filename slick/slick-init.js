// Слайдер по горизонтали

var newsSlider = $('.sl');

newsSlider.slick({
	infinite: true,
	slidesToShow: 3,
	slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1109,
      settings: {
        centerMode: false,
        variableWidth: true,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ],
  arrows: true,
  appendArrows: newsSlider.closest('.slider_news_row').find('.button_slider'),
  prevArrow: '<button class="button_arrow"><span></span></button>',
  nextArrow: '<button class="button_arrow"><span></span></button>'
});

// Слайдер на всю ширину

var hotSlider = $('.hot_slider_js');

hotSlider.slick({
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
  dots: false,
  arrows: true,
  responsive: [
    {
      breakpoint: 1109,
      settings: {
        centerMode: false
      }
    }
  ],
  appendArrows: hotSlider.closest('.section__hot_tours').find('.button_slider'),
  prevArrow: '<button class="button_arrow"><span></span></button>',
  nextArrow: '<button class="button_arrow"><span></span></button>'
});

var loveSlider = $('.love_rus_slider_js');

loveSlider.slick({
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
  dots: false,
  arrows: true,
  responsive: [
    {
      breakpoint: 1109,
      settings: {
        centerMode: false
      }
    }
  ],
  appendArrows: loveSlider.closest('.section__russian_love').find('.button_slider'),
  prevArrow: '<button class="button_arrow"><span></span></button>',
  nextArrow: '<button class="button_arrow"><span></span></button>'
});

// Слайдер по центру

$('.partners').slick({
  centerMode: true,
  centerPadding: '0px',
  slidesToShow: 4,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: false,
        centerPadding: '10px',
        slidesToShow: 1,
        slidesToShow: 1
      }
    },
    // {
    //   breakpoint: 480,
    //   settings: {
    //     arrows: false,
    //     centerMode: false,
    //     centerPadding: '0px',
    //     slidesToShow: 2,
    //     slidesToShow: 2
    //   }
    // }
  ]
});